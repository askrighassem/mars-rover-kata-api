using System.Net;
using MarsRoverDomain.Exceptions;
using Microsoft.VisualBasic;

namespace MarsRoverAPI.MarsRoverMiddlewares;

public class MarsRoverExceptionMiddleware
{
    private readonly RequestDelegate _next;


    public MarsRoverExceptionMiddleware(RequestDelegate next)
    {
        _next = next;

    }

    public async Task InvokeAsync(HttpContext httpContext)
    {
        try
        {
            await _next(httpContext);
        }
        catch (Exception exception)
        {

            await HandleExceptionAsync(httpContext, exception); 
        }
    }

    private async Task HandleExceptionAsync(HttpContext httpContext, Exception exception)
    {
        httpContext.Response.ContentType = "Application/json";
        switch(exception)
        {
            case CommandException :
                httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                
                break;
            default:
                httpContext.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
                break;
        }
        
      
        await httpContext.Response.WriteAsync(new ExceptionDetails()
        {
            StatusCode = httpContext.Response.StatusCode,
            Message = new CommandException().Message
        }.ToString());
    }
}