using MarsRoverAPI.MarsRoverMiddlewares;
using MarsRoverDomain;
using Microsoft.AspNetCore.Mvc;

#region Services
var builder = WebApplication.CreateBuilder(args);
builder.Services.AddScoped<IMarsRover,MarsRoverDomain.MarsRover>();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
var app = builder.Build();
#endregion

#region Middlewares
app.UseMiddleware<MarsRoverExceptionMiddleware>();
app.UseSwagger();
app.UseSwaggerUI();
#endregion

#region Controllers
app.MapGet("/", () => "Hello Mars Rover is alive .....");

app.MapPost("/executeCommands",
        (string commands, [FromServices] IMarsRover marsRover, HttpResponse response) =>
            marsRover.Execute(commands.ToUpper()))
    .WithName("Execute commands")
    .Produces<string>(StatusCodes.Status200OK)
    .Produces(StatusCodes.Status400BadRequest);
  
#endregion

app.Run();