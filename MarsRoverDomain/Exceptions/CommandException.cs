using MarsRover.Shared.Exceptions;

namespace MarsRoverDomain.Exceptions;

public class CommandException : MarsRoverException
{
    public CommandException() : base("Bad command exception allowed commands are : R r,L l,M m")
    {
            
    }
}