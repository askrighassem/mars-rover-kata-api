namespace MarsRoverDomain;


public sealed class Direction
    {
        public  char? Value;

        public Direction(char value)
        {
            Value = value; 
        }

        public Direction GoToTheLeft()
        {
            Value = Value switch
            {
                'N' => 'W',
                'W' => 'S',
                'S' => 'E',
                'E' => 'N',
                _ => null
            };
            return this;
        }
        public Direction GoToTheRight()
        {
            Value = Value switch
            {
                'N' => 'E',
                'E' => 'S',
                'S' => 'W',
                'W' => 'N',
                _ => null
            };
            return this; 
        }
        
    }