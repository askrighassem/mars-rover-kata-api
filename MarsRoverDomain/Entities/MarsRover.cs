﻿using System.Threading.Tasks.Dataflow;
using MarsRoverDomain.Exceptions;

namespace MarsRoverDomain;

public sealed class MarsRover : IMarsRover
{
    private const int MaxHeightFromOrigin = 10;
    private const int MaxWidthFromOrigin = 10; 
    private readonly Grid _grid = new Grid(MaxHeightFromOrigin,MaxWidthFromOrigin);  
    private Direction _direction = new Direction('N');
    private readonly Coordinate _coordinate = new Coordinate(0, 0);
     
    public string Execute(string commands)
    {
        foreach (var command in commands)
        {
            switch (command)
            {
                case 'R':
                    _direction = _direction.GoToTheRight();
                    break;
                case 'L':
                    _direction = _direction.GoToTheLeft();
                    break;
                case 'M' : 
                    MoveUp(_coordinate);
                    break;
                default:
                    throw new CommandException(); 
            }
        }
        return _coordinate.X+":"+_coordinate.Y+":" + _direction.Value;

    }

    private void MoveUp(Coordinate coordinate)
    {
        if ( _grid.RoverExceedLimit(coordinate))
        {
            WrapAround(coordinate); 
            return;
        }

        if (_direction.Value is 'N' or 'S')
            coordinate.Y++;
        else
            coordinate.X++;
    }
    private void WrapAround(Coordinate coordinate)
    {
        coordinate.X = 0;
        coordinate.Y = 0;
        Execute("RR");
    }
}