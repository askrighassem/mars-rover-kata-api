namespace MarsRoverDomain;

public sealed class Grid
{
    private readonly int _maxHeight;
    private readonly int _maxWidth;    
   

    public Grid(int maxHeightFromOrigin, int maxWidthFromOrigin)
    {
        _maxHeight = maxHeightFromOrigin ;
        _maxWidth = maxWidthFromOrigin ; 
    }

    public bool RoverExceedLimit(Coordinate coordinate)
    {

        var result  =  _maxWidth <= coordinate.X || _maxHeight <= coordinate.Y;
        return result; 
    }
}