namespace MarsRoverDomain;

public interface IMarsRover
{
    public string Execute(string commands); 
}