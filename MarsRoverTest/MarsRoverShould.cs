using FluentAssertions;
using MarsRoverDomain;
using Xunit;

namespace MarsRoverTest;

public class MarsRoverShould
{
    private readonly  MarsRoverDomain.MarsRover _rover; 
    public MarsRoverShould()
    {
         _rover = new MarsRoverDomain.MarsRover();
    }
    [Theory]
    [InlineData("R","0:0:E" )]
    [InlineData("RR","0:0:S" )]
    [InlineData("RRR","0:0:W" )]
    [InlineData("RRRR","0:0:N" )]
    
    public void RotateRight(string commands, string expectedValues)
    {
       
        var result = _rover.Execute(commands);
            result.Should().Be(expectedValues); 
    }
    
    [Theory]
    [InlineData("L","0:0:W" )]
    [InlineData("LL","0:0:S" )]
    [InlineData("LLL","0:0:E" )]
    [InlineData("LLLL","0:0:N" )]
    
    public void RotateLeft(string commands, string expectedValues)
    {
       
        var result = _rover.Execute(commands);
        result.Should().Be(expectedValues); 
    }
    
    [Theory]
    [InlineData("M","0:1:N" )]
    [InlineData("MM","0:2:N" )]
    [InlineData("MMM","0:3:N" )]
    [InlineData("MMMM","0:4:N" )]
    [InlineData("MMMMM","0:5:N" )]
    public void MoveUpOnYAxis(string commands, string expectedValues)
    {
       
        var result = _rover.Execute(commands);
        result.Should().Be(expectedValues); 
    }
    
    [Theory]
    [InlineData("RM","1:0:E" )]
    [InlineData("RMM","2:0:E" )]
    [InlineData("LM","1:0:W" )]
    [InlineData("LMM","2:0:W" )]
    public void MoveUpOnXAxis(string commands, string expectedValues)
    {
       
        var result = _rover.Execute(commands);
        result.Should().Be(expectedValues); 
    }
 
    
    
    [Theory]
    [InlineData("MMMMMMMMMMM","0:0:S" )]

    public void WrapAroundWhenItReachTheEndOfGrid(string commands, string expectedValues)
    {
       
        var result = _rover.Execute(commands);
        result.Should().Be(expectedValues); 
    }
}

