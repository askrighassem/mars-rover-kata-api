﻿namespace MarsRover.Shared.Exceptions;

public abstract class MarsRoverException : Exception
{
    protected MarsRoverException(string message) : base(message)
    {

    }
}