# Mars-Rover-Kata-Api

Develop an API that moves a rover around a grid.


Rules : 
1. You are given the initial starting point (0,0,N) of a rover.
2. 0,0 are X,Y co-coordinates on a grid of (10,10).
3. N is the direction it is facing (i.e. N,S,E,W).
4. L and R allow the rover to rotate left and right.
5. M allows the rover to move one point in the current direction.
6. The rover receives a char array of commands e.g RMMLM and returns the finishing point after the moves  e.g 2:1:N
7. The rover wraps around if it reaches the end of the grid.
8. The grid may have obstacles.If a given sequence of commands encounters an obstacle, the rover moves up to the last possible point and reports the obstacle e.g 0:2:2:N.

## Running the solution 
To run the test project : dotnet test \
To run the web api project : dotnet run  --project MarsRoverApi 

## Technical Environment
TDD Style : OutsideIn\
Platform : NET6\
Architecture : Clean 

## Explanation of technical choices
We have 3 possible commands 'L', 'R', 'M' and 4 directions 'N', 'E', 'S', 'W' which will not be changed in the future which allowed me to make a simple switch case to have an ease in the readability of the code .

Orders must be validated : "LK" is a false command so our rover will not be able to know the K command.

I implemented a specific exception named command exception and I injected a custom middleware that allows to manage this type of exception which have 400 status code and treated as a bad request.

Our Rover can accept lowercase commands.
    

